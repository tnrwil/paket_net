# Configure the Packet Provider. 
# webhook chaos
# automatic kickoff
#my test


provider "packet" {
  auth_token = "qPbpBDFtxge6uJdFc2b7TEDjkKVXmVVq"

}



# Declare your project ID
#
# You can find ID of your project form the URL in the Packet web app.
# For example, if you see your devices listed at
# https://app.packet.net/projects/352000fb2-ee46-4673-93a8-de2c2bdba33b
# .. then 352000fb2-ee46-4673-93a8-de2c2bdba33b is your project ID.
locals {
  project_id = "19014ee0-e0ae-4755-9521-ab83a584dec1"
}

# If you want to create a fresh project, you can create one with packet_project
# 
# resource "packet_project" "cool_project" {
#   name           = "My First Terraform Project"
# }


# Create a device and add it to tf_project_1
resource "packet_device" "demo_one" {
  hostname         =  "demoone"
  plan             = "c1.small.x86"
  facilities         = ["ewr1"]
  operating_system = "vmware_esxi_6_5"
  billing_cycle    = "hourly"
  project_id       = "${local.project_id}"
}

#output "demoone_ip_addr" {
#  value = packet_device.demo_one.network.0
#}


# Create a device and add it to tf_project_1
resource "packet_device" "demo_two" {
  hostname         =  "demotwo"
  plan             = "c1.small.x86"
  facilities         = ["ewr1"]
  operating_system = "vmware_esxi_6_5"
  billing_cycle    = "hourly"
  project_id       = "${local.project_id}"
  provisioner "local-exec" {
    #command = "terraform output -json demoone_ip_addr |  jq -r '.address'"
    #command = "echo ${packet_device.demo_two.project_id}"
  }
}

#output "demotwo_ip_addr" {
#  value = packet_device.demo_two.network.0
#}


# Create a device and add it to tf_project_1
#resource "packet_device" "web6" {
#  hostname         =  "web6"
#  plan             = "c1.small.x86"
#  facilities         = ["ewr1"]
#  operating_system = "ubuntu_16_04"
#  billing_cycle    = "hourly"
#  project_id       = "${local.project_id}"

  # if you have created project with packet_project resource, refer to its ID
  # project_id       = "${packet_project.cool_project.id}"
#}

# Create a device and add it to tf_project_1
#resource "packet_device" "web4" {
#  hostname         =  "web4"
#  plan             = "c1.small.x86"
#  facilities         = ["ewr1"]
#  operating_system = "ubuntu_16_04"
#  billing_cycle    = "hourly"
#  project_id       = "${local.project_id}"

  # if you have created project with packet_project resource, refer to its ID
  # project_id       = "${packet_project.cool_project.id}"
#}

# Create a device and add it to tf_project_1
#resource "packet_device" "web5" {
#  hostname         =  "web5"
#  plan             = "c1.small.x86"
#  facilities         = ["ewr1"]
#  operating_system = "ubuntu_16_04"
#  billing_cycle    = "hourly"
#  project_id       = "${local.project_id}"

  # if you have created project with packet_project resource, refer to its ID
  # project_id       = "${packet_project.cool_project.id}"
#}


#Deploy vms on exi server


