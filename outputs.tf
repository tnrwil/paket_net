output "demoone" {
  description = "demoone ip"
  value       = "${packet_device.demo_one.network.0}"
}
output "demotwo" {
  description = "demotwo ip"
  value       = "${packet_device.demo_two.network.0}"
}